#!/usr/bin/env bash
#
# This example sets up 15 commands
#
# It sets verbose output (so that you get a nice summary at the end).
#
# Each command is expected to match a specific, randomly selected exit code (zero or one).
#
# Each command must meet the expectation twice in a row. With this many commands and consecutive calls, simple random execution would likely take a long time to complete.
#
# Each execution of each command waits a random amount of time up to 5 seconds.
#
# Each command latches (stops/pauses) after meeting its expectations.

commandArgs=( npx exit-code-monitor -v -m code -x 2 --latch )
for ((x=1; x<=15; x++)); do
  commandArgs=( "${commandArgs[@]}" -l "command $x" -c $((RANDOM % 2)) "sleep \$((RANDOM % 6)) ; exit \$((RANDOM % 2))" )
done
set -o xtrace
"${commandArgs[@]}"
true exit-code-monitor exited with code $? # xtrace will print this, but the true command will ignore the arguments
