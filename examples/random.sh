#!/usr/bin/env bash
#
# This example sets up 5 commands
#
# It sets verbose output (so that you get a nice summary at the end).
#
# Each command is expected to match a specific, randomly selected exit code (zero or one).
#
# Each execution of each command waits a random amount of time up to 5 seconds.
#
# Press 'q' or Ctrl-C (interrupt) at any time to see what an early exit of the
# script looks like, or wait (typically no more than a minute or two) for the
# script to exit successfully on its own.

commandArgs=( npx exit-code-monitor -v -m code )
for ((x=1; x<=5; x++)); do
  commandArgs=( "${commandArgs[@]}" -l "command $x" -c $((RANDOM % 2)) "sleep \$((RANDOM % 6)) ; exit \$((RANDOM % 2))" )
done
set -o xtrace
"${commandArgs[@]}"
true exit-code-monitor exited with code $? # xtrace will print this, but the true command will ignore the arguments
