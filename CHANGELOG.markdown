# Change log

## v1.1.3

* Updated build image to use node version 22
* Modified `engines` spec to allow newer versions of `npm`.
* Updated dependencies to latest versions.
* Fixed bug with unit test where `PATH` with duplicates at time of test caused
  test to break.
* Ran `npm pkg fix`.

## v1.1.2

* Accidentally tagged without updating `package.json`, so skipping this number.

## v1.1.1

* Forgot to regenerate README Usage section for npm page

## v1.1.0

* Feature: Added interactive mode option to stop/pause individual runners. Just
  press enter or space on the highlighted line. Navigate up/down with arrow or
  vim keys.
* Feature: Added `--latch` option to stop running the command after the first
  time it meets its expectations the required number of times. Note: This
  feature isn't generally recommended because a flaky command/status will not
  be retested, leading to a higher likelihood of a false positive state, but
  it's useful for reducing the number of times unnecessary commands are run
  (for example if you're testing for the presence of a file).

## v1.0.0

* Split examples into separate directory.
* Note: No breaking changes, but change in license suggests bumping major
  version number.
* Changed license from [WTFPL](http://www.wtfpl.net/) to
  [Unlicense](https://unlicense.org/) because
  [GitLab and OSI believe it has some problems](https://gitlab.gnome.org/GNOME/gnome-builder/-/issues/666).
  The intent was to be as permissive as possible, and the Unlicense is an
  [OSI approved](https://opensource.org/licenses/unlicense),
  [SPDX listed](https://spdx.org/licenses/) license which accomplishes the same
  goal.

## v0.4.0

* Winston logging

## v0.3.10

* Unit tests completed
* Added short contribution guide
* Bug fix: Short summary line would report commands meeting but without enough
  consecutive calls as met.

## v0.3.9

* npmjs.com is about 7 characters too narrow (approximately enough space for 73
  mono spaced characters). Regenerated with 70-character width so it fits
  without horizontal scrolling needed.

## v0.3.8

* GitLab unit test integration support

## v0.3.7

* Code coverage reporting in GitLab
* Added badges/shields to README

## v0.3.6

* Removed unnecessary `package.json` keywords
* Set up GitLab CI

## v0.3.5

* Bug fix: Writes to PrefixStream ending in newline wouldn't start next line
  with prefix.
* Bug fix: Resolve file path before opening write streams to prevent duplicate
  file descriptors when route to file is different but file is the same (e.g.
  foo/../x and bar/../x only open one file descriptor).
* Refactoring for unit testing and about halfway done with writing tests.

## v0.3.4

* Indicate by color if consecutive count is met

## v0.3.3

* Documentation improvement
* Bug fix: Exit code zero if all command modes are info
* Bug fix: TTY wouldn't allow exit-code-monitor to exit

## v0.3.2

* Limit TTY updates to a max frequency to prevent terminal overload, fixed
  package URLs

## v0.3.1

* Bug fix

## v0.3.0

* If all commands in info mode, don't auto-exit

## v0.2.1

* Bug fix

## v0.2.0

* Option to set minimum consecutive executions that must match expectations, so
  that you can catch flaky results

## v0.1.1

* Bug fix

## v0.1.0

* First publish
