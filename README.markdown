# Exit Code Monitor

Command line tool for repeatedly calling and monitoring the exit status of a
set of commands. Useful for waiting for a set of conditions to occur before
performing an operation.

[![Version](https://img.shields.io/npm/v/exit-code-monitor.svg)](https://www.npmjs.com/package/exit-code-monitor?activeTab=versions)
[![Downloads](https://img.shields.io/npm/dm/exit-code-monitor.svg)](https://www.npmjs.com/package/exit-code-monitor)
[![Package Quality](https://packagequality.com/shield/exit-code-monitor.svg)](https://packagequality.com/#?package=exit-code-monitor)
[![Pipeline Status](https://gitlab.com/zannalov/exit-code-monitor/badges/main/pipeline.svg)](https://gitlab.com/zannalov/exit-code-monitor/-/pipelines)
[![Coverage](https://gitlab.com/zannalov/exit-code-monitor/badges/main/coverage.svg?job=test)](https://gitlab.com/zannalov/exit-code-monitor/-/jobs/artifacts/main/file/coverage/lcov-report/index.html?job=test)

## Usage

```text
Each non-option argument is interpreted as a command to be repeatedly
executed in a subshell until all provided commands meet their
expectations. Commands are run within a subshell. Please be careful of
command injection!

Target state
  -m, --mode         How the commands following this option are
                     expected to exit:

                     pass|p expects a zero exit code.

                     fail|f expects a non-zero exit code (and not
                     timeout).

                     code|c expects a specific exit code (see
                     --code|-c).

                     info|i expects any exit code. The command will
                     be run/displayed, but any exit code allows the
                     monitor to exit. If all commands have this
                     mode, exit-code-monitor will not auto-exit.

     [string] [choices: "pass", "p", "fail", "f", "code", "c", "info",
                                                "i"] [default: "pass"]
  -c, --code         Command is expected to return this exit code
                     NOTE: ONLY USED IF --mode=code           [number]
  -x, --consecutive  Command must meet expectations this many times in
                     a row [minimum: 1]          [number] [default: 1]
  -H, --latch        Command will stop running the first time it meets
                     all of its expectations. In interactive mode it
                     can be restarted by pressing enter or space with
                     the command highlighted.
                                            [boolean] [default: false]

Identification
  -l, --label  Set the short string to show for the next command (only
               applies to next command)
                             [string] [default: first word of command]

Timing
  -t, --timeout      After this many seconds send kill signal to
                     command and use timeout exit code [minimum: 1]
                                                [number] [default: 30]
      --timeoutcode  When timeout is reached use this exit code (may
                     also be "null" to prevent timeout from matching
                     any expectation)         [number] [default: null]
  -d, --delay        When command exits, delay this many seconds
                     before re-running command [minimum: 1]
                                                 [number] [default: 1]

Command Context
  -p, --pwd     Process working directory when starting command
                                            [string] [default: $(pwd)]
  -P, --path    Prepend PATH with this string[string] [default: $PATH]
  -o, --stdout  stdout of commands will be formatted "$title:
                $stdoutLine" and appended to the specified file
                                         [string] [default: /dev/null]
  -e, --stderr  stderr of commands will be formatted "$title:
                $stderrLine" and appended to the specified file
                                         [string] [default: /dev/null]

Output
  -T, --tty          Force TTY display on or off
                                         [boolean] [default: detected]
      --color        Force color when in non-TTY mode
                                            [boolean] [default: false]
  -r, --report       When not using TTY output, emit a report this
                     frequently in seconds [minimum: 1]
                                                [number] [default: 10]
  -R, --rate         When using TTY output, refresh the screen at
                     LEAST every so many milliseconds [minimum: 100]
                                               [number] [default: 250]
  -L, --limit        When using TTY output, refresh the screen at MOST
                     every so many milliseconds [minimum: 100]
                                               [number] [default: 100]
  -v, --verbose      When not using TTY output, make each emitted
                     update multiline (default is a single line per
                     udpate)                                 [boolean]
  -q, --quiet        Do not show anything on the screen, only return
                     exit code when interrupted or complete  [boolean]
  -C, --commandterm  Use this word to represent the commands specified
                     to be watch.        [string] [default: "Command"]
  -M, --metterm      Use this word to represent commands which are
                     meeting their expectations.
                                            [string] [default: "GOOD"]
  -U, --unmetterm    Use this word to represent commands that are not
                     meeting thier expectations.
                                             [string] [default: "BAD"]
  -W, --waitterm     Use this word when we have not yet received any
                     result for a command.
                                         [string] [default: "PENDING"]
  -g, --log          Write a jsonl winston log to the provided file
                     name.             [string] [default: "/dev/null"]
  -G, --level        What winston log level to use.
        [string] [choices: "error", "warn", "info", "http", "verbose",
                                  "debug", "silly"] [default: "silly"]

Options:
  -h, --help     Show help                                   [boolean]
  -V, --version  Show version number                         [boolean]

Examples:

Wait for all commands to succeed:

$ npx exit-code-monitor command1 command2 command3

Wait for all commands to fail:

$ npx exit-code-monitor --mode=fail command1 command2 command3

Wait for commands 1, 2, 5, and 6 to succeed, and for 3 and 4 to fail

$ npx exit-code-monitor \
  --mode=pass command1 command2 \
  --mode=fail command3 command4 \
  --mode=pass command5 command6

Note that the above can be written more concisely this way, but that
the commands will then be listed in the order 1 2 5 6 3 4:

$ npx exit-code-monitor \
  command1 command2 command5 command6 \
  --mode=fail command3 command4

Wait for the command to have exit code 123 using short options:

$ npx exit-code-monitor -m c -c 123 command1
```
