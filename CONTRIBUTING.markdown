# Contribution Guide

If you've found a problem, please submit an issue.

If you have a suggested code change, please

1. Fork the repository
2. Make changes
3. Ensure unit tests are written to cover the change/scenario
4. Run `npm run lint` and `npm run test` and resolve any problems
5. Commit and push your changes
6. Submit a merge request
