const { $, red, green, cyan, yellow, magenta, bgBlack } = require('kleur/colors')

const { Runner } = require('./Runner')
const { formatRuntime } = require('./util')

const detailedReport = ({ started, options, runners }) => {
  const now = Date.now()
  return runners.map(runner => {
    const row = [
      options.commandterm,
      yellow(runner.spec.label)
    ]

    if (runner.last) {
      if (runner.last.result === Runner.EXIT_RESULT_MET) {
        row.push(green(options.metterm))
      } else if (runner.last.result === Runner.EXIT_RESULT_UNMET) {
        row.push(red(options.unmetterm))
      }
    } else {
      row.push(cyan(options.waitterm))
    }

    if (runner.spec.consecutive > 1) {
      if (runner.consecutive < runner.spec.consecutive) {
        row.push(red(runner.consecutive + ' times in a row'))
      } else {
        row.push(green(runner.consecutive + ' times in a row'))
      }
    }

    if (runner.last) {
      if (runner.last.state === Runner.EXIT_STATE_TIMEOUT) {
        row.push(red('timeout'))
      } else if (runner.last.state === Runner.EXIT_STATE_EXITED) {
        row.push(cyan('with exit code ' + String(runner.last.code)))
      } else {
        row.push(red('ERROR'))
      }
    }

    row.push('at ' + magenta(formatRuntime(started, (runner.firstMet || now))))

    return row.join(' ')
  }).join('\n')
}

const addToSummaryArray = (summary, color, term, runners) => {
  if (runners.length) {
    summary.push(color(`${term}: ${runners.join(', ')}`))
  }
}

const summaryLine = ({ options, runners }) => {
  const pending = runners
    .filter(runner => !runner.last?.result)
    .map(runner => runner.spec.label)

  const ignored = runners
    .filter(runner => runner.last?.result === Runner.EXIT_RESULT_IGNORE)
    .map(runner => runner.spec.label)

  const met = runners
    .filter(runner =>
      runner.last?.result === Runner.EXIT_RESULT_MET &&
      runner.consecutive >= runner.spec.consecutive
    )
    .map(runner => runner.spec.label)

  const unmet = runners
    .filter(runner =>
      runner.last && // not pending
      (
        runner.last.result === Runner.EXIT_RESULT_UNMET || // explicitly unmet or
        ( // not ignored and not enough consecutive results
          runner.last.result !== Runner.EXIT_RESULT_IGNORE &&
          runner.consecutive < runner.spec.consecutive
        )
      )
    )
    .map(runner => runner.spec.label)

  const summary = [options.commandterm]
  addToSummaryArray(summary, red, options.unmetterm, unmet)
  addToSummaryArray(summary, green, options.metterm, met)
  addToSummaryArray(summary, yellow, options.waitterm, pending)
  addToSummaryArray(summary, cyan, 'Ignored', ignored)
  return summary.join(' ')
}

const setColor = ({ options }) => {
  $.enabled = Boolean(options.tty || options.color)
}

const printExitSummary = (details) => {
  module.exports.setColor(details)

  if (details.options.verbose) {
    console.log(bgBlack(module.exports.detailedReport(details)))
  } else {
    console.log(bgBlack(module.exports.summaryLine(details)))
  }
}

class NonInteractive {
  constructor ({ started, options, runners, exit }) {
    this.started = started
    this.options = options
    this.runners = runners
    this.exit = exit

    this.updateIntervalHandle = null

    this.printReport = this.printReport.bind(this)
  }

  setup () {
    module.exports.setColor(this)

    this.updateIntervalHandle = setInterval(
      this.printReport,
      this.options.report * 1000
    )

    process.on('SIGTERM', this.exit)
    process.on('SIGINT', this.exit)
    process.on('SIGBREAK', this.exit)
    process.on('SIGUSR2', this.printReport)
  }

  printReport () {
    if (this.options.verbose) {
      console.log((new Date()).toLocaleString())
      console.log(module.exports.detailedReport(this))
      console.log()
    } else {
      console.log('Waiting for ' + module.exports.summaryLine(this))
    }
  }

  shutdown () {
    clearInterval(this.updateIntervalHandle)
    process.off('SIGTERM', this.exit)
    process.off('SIGINT', this.exit)
    process.off('SIGBREAK', this.exit)
    process.off('SIGUSR2', this.printReport)
  }
}

module.exports = {
  NonInteractive,
  addToSummaryArray,
  detailedReport,
  printExitSummary,
  setColor,
  summaryLine
}
