/* eslint-env jest */
jest.mock('kleur/colors')
jest.mock('./util')

const colors = require('kleur/colors')
const { AppSimulation } = require('./__fixtures__/AppSimulation')
const { formatRuntime } = require('./util')

const subject = require('./NonInteractive')

let app

beforeEach(() => {
  // Simulate execution time 2.345s after start time
  const ended = 2346
  jest.spyOn(Date, 'now')
  Date.now.mockReturnValue(ended)

  // Simulate output of locale date
  jest.spyOn(Date.prototype, 'toLocaleString')
  Date.prototype.toLocaleString.mockReset() // don't call original method
  Date.prototype.toLocaleString.mockReturnValue('LOCALE_FORMATTED_DATE_HERE')

  // Mock all colors to return a formatted string we can reasonably read in snapshots
  Object.keys(colors).forEach(color => {
    if (colors[color]?.mockImplementation) {
      colors[color].mockReset()
      colors[color].mockImplementation(
        str => `[[${color}]]${str}[[/${color}]]`
      )
    }
  })

  // Show that we called the method instead of showing the formatted result
  // (assume the testing of the method covers actual formatting)
  formatRuntime.mockReset()
  formatRuntime.mockImplementation(
    (started, ended) => `formatRuntime(${ended - started}ms)`
  )

  // Simulate app
  app = new AppSimulation()
  app.updateEndTimes(ended)

  // Output doesn't include details about running command
  app.removeDelayRunners()
})

afterEach(() => {
  Date.prototype.toLocaleString.mockRestore()
  Date.now.mockRestore()
})

describe('detailedReport', () => {
  it('should produce the expected output', () => {
    expect(subject.detailedReport(app)).toMatchSnapshot()
  })
})

describe('addToSummaryArray', () => {
  it('should add the provided runners to the summary array wrapped in the provided color and prefixed with the provided term', () => {
    const summary = []
    const color = (str) => (`++${str}--`)
    const term = 'Stuff'
    const runners = ['a', 'b', 'c']

    subject.addToSummaryArray(summary, color, term, runners)

    expect(summary).toEqual([
      '++Stuff: a, b, c--'
    ])
  })

  it('should not alter the summary array if there are no runners to be reported', () => {
    const summary = ['foo']
    const color = (str) => (`++${str}--`)
    const term = 'Stuff'
    const runners = []

    subject.addToSummaryArray(summary, color, term, runners)

    expect(summary).toEqual(['foo'])
  })
})

describe('summaryLine', () => {
  it('should produce the expected output', () => {
    expect(subject.summaryLine(app)).toMatchSnapshot()
  })
})

describe('setColor', () => {
  beforeEach(() => {
    colors.$.enabled = null
  })

  it('should explicitly enable color on tty', () => {
    subject.setColor({ options: { tty: true, color: false } })
    expect(colors.$.enabled).toBe(true)
  })

  it('should explicitly enable color if requested', () => {
    subject.setColor({ options: { tty: false, color: true } })
    expect(colors.$.enabled).toBe(true)
  })

  it('should explicitly disable color if neither tty nor color option are set', () => {
    subject.setColor({ options: { tty: false, color: false } })
    expect(colors.$.enabled).toBe(false)
  })
})

describe('printExitSummary', () => {
  beforeEach(() => {
    jest.spyOn(subject, 'setColor')
    jest.spyOn(subject, 'detailedReport')
    jest.spyOn(subject, 'summaryLine')
    jest.spyOn(console, 'log')

    console.log.mockReset() // don't call original method

    subject.detailedReport.mockReset() // don't call original method
    subject.detailedReport.mockReturnValue('A_DETAILED_REPORT')

    subject.summaryLine.mockReset() // don't call original method
    subject.summaryLine.mockReturnValue('A_SUMMARY_LINE')
  })

  afterEach(() => {
    console.log.mockRestore()
    subject.summaryLine.mockRestore()
    subject.detailedReport.mockRestore()
    subject.setColor.mockRestore()
  })

  it('should enable or disable coloration', () => {
    subject.printExitSummary(app)

    expect(subject.setColor).toHaveBeenCalledTimes(1)
    expect(subject.setColor).toHaveBeenCalledWith(app)
  })

  it('should console log a summary line by default', () => {
    app.options.verbose = false

    subject.printExitSummary(app)

    expect(subject.detailedReport).not.toHaveBeenCalled()
    expect(subject.summaryLine).toHaveBeenCalledTimes(1)
    expect(console.log.mock.calls).toMatchSnapshot()
  })

  it('should console log a detailed report if verbose enabled', () => {
    app.options.verbose = true

    subject.printExitSummary(app)

    expect(subject.detailedReport).toHaveBeenCalledTimes(1)
    expect(subject.summaryLine).not.toHaveBeenCalled()
    expect(console.log.mock.calls).toMatchSnapshot()
  })
})

describe('NonInteractive', () => {
  beforeEach(() => {
    jest.useFakeTimers()

    jest.spyOn(subject, 'setColor')

    jest.spyOn(process, 'on')
    process.on.mockReset() // don't call original method

    jest.spyOn(process, 'off')
    process.off.mockReset() // don't call original method

    jest.spyOn(console, 'log')
    console.log.mockReset() // don't call original method
  })

  afterEach(() => {
    console.log.mockRestore()
    process.off.mockRestore()
    process.on.mockRestore()
    subject.setColor.mockRestore()

    jest.clearAllTimers()
    jest.useRealTimers()
  })

  describe('constructor', () => {
    it('should cache app details', () => {
      const display = new subject.NonInteractive(app)
      expect(display.started).toBe(app.started)
      expect(display.options).toBe(app.options)
      expect(display.runners).toBe(app.runners)
      expect(display.exit).toBe(app.exit)
    })

    it('should not affect anything until started', () => {
      /* eslint-disable no-unused-vars */
      const display = new subject.NonInteractive(app)
      /* eslint-enable no-unused-vars */

      expect(subject.setColor).not.toHaveBeenCalled()
      expect(jest.getTimerCount()).toBe(0)
      expect(process.on).not.toHaveBeenCalled()
      expect(console.log).not.toHaveBeenCalled()
    })
  })

  describe('startup', () => {
    it('should set coloration', () => {
      const display = new subject.NonInteractive(app)
      display.setup()

      expect(subject.setColor).toHaveBeenCalledTimes(1)
      expect(subject.setColor).toHaveBeenCalledWith(
        expect.objectContaining({
          options: app.options
        })
      )
    })

    it('should schedule the report', () => {
      const display = new subject.NonInteractive(app)
      display.printReport = jest.fn()

      display.setup()

      expect(jest.getTimerCount()).toBe(1)
      expect(display.printReport).not.toHaveBeenCalled()

      jest.advanceTimersByTime((app.options.report * 1000) - 1)
      expect(display.printReport).not.toHaveBeenCalled()

      jest.advanceTimersByTime(1)
      expect(display.printReport).toHaveBeenCalledTimes(1)

      jest.advanceTimersByTime(app.options.report * 1000)
      expect(display.printReport).toHaveBeenCalledTimes(2)
    })

    describe.each([
      'SIGTERM',
      'SIGINT',
      'SIGBREAK'
    ])('on %s', signal => {
      it('should trigger an exit', () => {
        const display = new subject.NonInteractive(app)

        display.setup()

        expect(process.on).toHaveBeenCalledWith(signal, app.exit)
      })
    })

    describe('on SIGUSR2', () => {
      it('should produce a report', () => {
        const display = new subject.NonInteractive(app)

        display.setup()

        expect(process.on).toHaveBeenCalledWith('SIGUSR2', display.printReport)
      })
    })
  })

  describe('printReport', () => {
    beforeEach(() => {
      jest.spyOn(subject, 'summaryLine')
      subject.summaryLine.mockReset() // don't call original method
      subject.summaryLine.mockReturnValue('{result of summaryLine() here}')

      jest.spyOn(subject, 'detailedReport')
      subject.detailedReport.mockReset() // don't call original method
      subject.detailedReport.mockReturnValue('{result of detailedReport() here}')
    })

    afterEach(() => {
      subject.detailedReport.mockRestore()
      subject.summaryLine.mockRestore()
    })

    it('should print a non-verbose summary line by default', () => {
      app.options.verbose = false
      const display = new subject.NonInteractive(app)
      display.setup()

      display.printReport()

      expect(console.log.mock.calls).toMatchSnapshot()
      expect(subject.summaryLine).toHaveBeenCalledWith(display)
    })

    it('should print a verbose summary line if requested', () => {
      app.options.verbose = true
      const display = new subject.NonInteractive(app)
      display.setup()

      display.printReport()

      expect(console.log.mock.calls).toMatchSnapshot()
      expect(subject.detailedReport).toHaveBeenCalledWith(display)
    })
  })

  describe('shutdown', () => {
    it('should clean up the timer', () => {
      const display = new subject.NonInteractive(app)

      display.setup()
      display.shutdown()

      expect(jest.getTimerCount()).toBe(0)
    })

    it('should unbind all signals', () => {
      const display = new subject.NonInteractive(app)

      display.setup()
      display.shutdown()

      expect(process.off).toHaveBeenCalledWith('SIGTERM', app.exit)
      expect(process.off).toHaveBeenCalledWith('SIGINT', app.exit)
      expect(process.off).toHaveBeenCalledWith('SIGBREAK', app.exit)
      expect(process.off).toHaveBeenCalledWith('SIGUSR2', display.printReport)
    })
  })
})
