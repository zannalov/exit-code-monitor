/* eslint-env jest */
jest.mock('child_process')

const { spawn } = require('child_process')
const { pickBy, uniq } = require('lodash')
const { Runner } = require('./Runner')

const mockLogger = () => ({
  log: jest.fn(),
  silly: jest.fn(),
  debug: jest.fn(),
  verbose: jest.fn(),
  http: jest.fn(),
  info: jest.fn(),
  warn: jest.fn(),
  error: jest.fn()
})

describe('Runner', () => {
  describe('static properties', () => {
    describe.each([
      'KILL_SIGNAL',
      'EXIT_STATE_EXITED',
      'EXIT_STATE_TIMEOUT',
      'EXIT_STATE_OTHER',
      'EXIT_CODE_CLASS_PASS',
      'EXIT_CODE_CLASS_FAIL',
      'EXIT_RESULT_MET',
      'EXIT_RESULT_UNMET',
      'EXIT_RESULT_IGNORE'
    ])('%s', name => {
      it('should be present', () => {
        expect(Runner[name]).toBeTruthy()
      })
    })

    describe('KILL_SIGNAL', () => {
      it('should be a string beginning with SIG', () => {
        expect(Runner.KILL_SIGNAL).toMatch(/^SIG/)
      })
    })

    describe('EXIT constants', () => {
      it('should have unique values', () => {
        const exitConstants = pickBy(
          Runner,
          (value, key) => /^EXIT_/.test(key)
        )
        const values = Object.values(exitConstants)

        expect(values).toEqual(uniq(values))
      })
    })
  })

  describe('constructor', () => {
    it('should set initial values', () => {
      const spec = 'sampleSpec'
      const runner = new Runner(spec, mockLogger())

      expect(runner).toMatchObject({
        killSignal: Runner.KILL_SIGNAL,
        spec,
        running: false,
        last: null,
        consecutive: 0,
        current: null,
        delayHandle: null,
        firstMet: null
      })
    })
  })

  describe('logProperties', () => {
    it('should return a serializable object of details about this runner', () => {
      const started = Date.now()
      const spec = {
        test: 123,
        stdout: 'PrefixStreamHere',
        stderr: 'PrefixStreamHere'
      }

      const runner = new Runner(spec, mockLogger())
      runner.current = {
        promise: Promise.resolve(),
        child: {
          connected: true,
          exitCode: null,
          killed: false,
          pid: 1234,
          signalCode: null,
          spawnArgs: ['foo'],
          spawnFile: 'something',
          stderr: 'pipe',
          stdin: 'pipe',
          stdio: [
            'pipe',
            'pipe',
            'pipe'
          ],
          stdout: 'pipe'
        },
        kill: jest.fn(),
        started
      }

      const obj = runner.logProperties()

      expect(obj).toStrictEqual({
        killSignal: Runner.KILL_SIGNAL,
        running: false,
        last: null,
        consecutive: 0,
        firstMet: null,
        current: {
          started,
          child: {
            pid: 1234
          }
        },
        spec: {
          test: 123
        }
      })
    })
  })

  describe('determineState', () => {
    it('should interpret timeout as the given timeoutcode', () => {
      const spec = { timeoutcode: 123 }
      const runner = new Runner(spec, mockLogger())
      const result = { timedOut: true }

      expect(runner.determineState(result)).toMatchObject({
        state: Runner.EXIT_STATE_EXITED,
        code: spec.timeoutcode
      })
    })

    it('should report timeout if not given timeoutcode', () => {
      const spec = { timeoutcode: NaN }
      const runner = new Runner(spec, mockLogger())
      const result = { timedOut: true }

      expect(runner.determineState(result)).toMatchObject({
        state: Runner.EXIT_STATE_TIMEOUT,
        code: NaN
      })
    })

    it('should report unexpected errors as state OTHER', () => {
      const spec = { timeoutcode: NaN }
      const runner = new Runner(spec, mockLogger())
      const result = { timedOut: false, err: 'something' }

      expect(runner.determineState(result)).toMatchObject({
        state: Runner.EXIT_STATE_OTHER,
        code: NaN
      })
    })

    it('should report invalid exit codes as state OTHER', () => {
      const spec = { timeoutcode: NaN }
      const runner = new Runner(spec, mockLogger())
      const result = { timedOut: false, code: null }

      expect(runner.determineState(result)).toMatchObject({
        state: Runner.EXIT_STATE_OTHER,
        code: NaN
      })
    })

    it('should report a valid exit code as exited with that code', () => {
      const spec = { timeoutcode: NaN }
      const runner = new Runner(spec, mockLogger())
      const result = { timedOut: false, code: 123 }

      expect(runner.determineState(result)).toMatchObject({
        state: Runner.EXIT_STATE_EXITED,
        code: result.code
      })
    })
  })

  describe('determineCodeClass', () => {
    it('should count exit code zero as passing', () => {
      const runner = new Runner({}, mockLogger())
      expect(runner.determineCodeClass(0)).toBe(Runner.EXIT_CODE_CLASS_PASS)
    })

    it('should count any other code as failing', () => {
      const runner = new Runner({}, mockLogger())

      // If something weird happens and we don't have a code
      expect(runner.determineCodeClass(undefined)).toBe(Runner.EXIT_CODE_CLASS_FAIL)
      expect(runner.determineCodeClass(null)).toBe(Runner.EXIT_CODE_CLASS_FAIL)
      expect(runner.determineCodeClass(NaN)).toBe(Runner.EXIT_CODE_CLASS_FAIL)

      // Or for any other possible exit code
      for (let x = 1; x <= 255; x++) {
        expect(runner.determineCodeClass(x)).toBe(Runner.EXIT_CODE_CLASS_FAIL)
      }
    })
  })

  describe('interpretResult', () => {
    describe.each([
      {
        mode: 'pass',
        specCode: null,
        state: Runner.EXIT_STATE_EXITED,
        code: 0,
        codeClass: Runner.EXIT_CODE_CLASS_PASS,
        expected: Runner.EXIT_RESULT_MET
      },
      {
        mode: 'pass',
        specCode: null,
        state: Runner.EXIT_STATE_EXITED,
        code: 1,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_UNMET
      },
      {
        mode: 'pass',
        specCode: null,
        state: Runner.EXIT_STATE_OTHER,
        code: null,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_UNMET
      },
      {
        mode: 'fail',
        specCode: null,
        state: Runner.EXIT_STATE_EXITED,
        code: 0,
        codeClass: Runner.EXIT_CODE_CLASS_PASS,
        expected: Runner.EXIT_RESULT_UNMET
      },
      {
        mode: 'fail',
        specCode: null,
        state: Runner.EXIT_STATE_EXITED,
        code: 1,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_MET
      },
      {
        mode: 'fail',
        specCode: null,
        state: Runner.EXIT_STATE_OTHER,
        code: null,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_UNMET
      },
      {
        mode: 'code',
        specCode: 10,
        state: Runner.EXIT_STATE_EXITED,
        code: 10,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_MET
      },
      {
        mode: 'code',
        specCode: 10,
        state: Runner.EXIT_STATE_EXITED,
        code: 1,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_UNMET
      },
      {
        mode: 'code',
        specCode: 10,
        state: Runner.EXIT_STATE_OTHER,
        code: null,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_UNMET
      },
      {
        mode: 'info',
        specCode: null,
        state: Runner.EXIT_STATE_EXITED,
        code: 0,
        codeClass: Runner.EXIT_CODE_CLASS_PASS,
        expected: Runner.EXIT_RESULT_IGNORE
      },
      {
        mode: 'info',
        specCode: null,
        state: Runner.EXIT_STATE_EXITED,
        code: 1,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_IGNORE
      },
      {
        mode: 'info',
        specCode: null,
        state: Runner.EXIT_STATE_OTHER,
        code: null,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        expected: Runner.EXIT_RESULT_IGNORE
      },
      {
        mode: 'fallback',
        specCode: null,
        state: Runner.EXIT_STATE_EXITED,
        code: 0,
        codeClass: Runner.EXIT_CODE_CLASS_PASS,
        expected: Runner.EXIT_RESULT_UNMET
      }
    ])('$mode $state $code $codeClass', ({ mode, specCode, state, code, codeClass, expected }) => {
      it('should return' + expected, () => {
        const runner = new Runner({ mode, code: specCode }, mockLogger())
        expect(runner.interpretResult({ state, code, codeClass })).toBe(expected)
      })
    })
  })

  describe('shouldKeepGoing', () => {
    let last

    beforeEach(() => {
      last = {
        state: Runner.EXIT_STATE_EXITED,
        code: 0,
        codeClass: Runner.EXIT_CODE_CLASS_PASS,
        result: Runner.EXIT_RESULT_MET,
        started: Date.now(),
        ended: Date.now()
      }
    })

    it('should return true if the runner has no last result', () => {
      const runner = new Runner({ consecutive: 1 }, mockLogger())
      expect(runner.shouldKeepGoing()).toBe(true)
    })

    it('should return true if the runner is unmet', () => {
      const runner = new Runner({ consecutive: 1 }, mockLogger())
      runner.consecutive = 1
      runner.last = last
      runner.last.code = 1
      runner.last.codeClass = Runner.EXIT_CODE_CLASS_FAIL
      runner.last.result = Runner.EXIT_RESULT_UNMET

      expect(runner.shouldKeepGoing()).toBe(true)
    })

    it('should return true if the runner consecutive count is less than the target', () => {
      const runner = new Runner({ consecutive: 2 }, mockLogger())
      runner.consecutive = 1
      runner.last = last

      expect(runner.shouldKeepGoing()).toBe(true)
    })

    it('should return false if the runner meets its consecutive count and is meeting its results', () => {
      const runner = new Runner({ consecutive: 2 }, mockLogger())
      runner.consecutive = 2
      runner.last = last

      expect(runner.shouldKeepGoing()).toBe(false)
    })

    it('should return false if the runner meets its consecutive count and is ignoring its results', () => {
      const runner = new Runner({ consecutive: 2 }, mockLogger())
      runner.consecutive = 2
      runner.last = last
      runner.last.code = 1
      runner.last.codeClass = Runner.EXIT_CODE_CLASS_FAIL
      runner.last.result = Runner.EXIT_RESULT_IGNORE

      expect(runner.shouldKeepGoing()).toBe(false)
    })
  })

  describe('runCommand', () => {
    let spec
    let child

    beforeEach(() => {
      jest.useFakeTimers()

      jest.spyOn(Date, 'now')

      jest.spyOn(process, 'kill')
      process.kill.mockImplementation(() => {})

      child = {
        stdout: { pipe: jest.fn() },
        stderr: { pipe: jest.fn() },
        pid: 123,
        on: jest.fn()
      }
      spawn.mockReset()
      spawn.mockReturnValue(child)

      spec = {
        command: 'command to be run',
        pwd: '/some/path',
        path: 'a:b:c',
        // by default no stdout
        // by default no stderr
        timeout: 30
      }
    })

    afterEach(() => {
      process.kill.mockRestore()
      Date.now.mockRestore()
      jest.clearAllTimers()
      jest.useRealTimers()
    })

    it('should record the start time', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(Date.now).toHaveBeenCalledTimes(1)
      expect(runner.current.started).toBe(Date.now.mock.results[0].value)
    })

    describe('with space in command', () => {
      it('should spawn the command in a subshell', () => {
        const runner = new Runner(spec, mockLogger())
        runner.runCommand()
        expect(spawn).toHaveBeenCalledTimes(1)
        expect(spawn).toHaveBeenCalledWith(
          'command to be run',
          expect.objectContaining({
            shell: true
          })
        )
      })
    })

    describe('without space in command', () => {
      it('should not spawn the command in a subshell', () => {
        spec.command = 'no-spaces'

        const runner = new Runner(spec, mockLogger())
        runner.runCommand()
        expect(spawn).toHaveBeenCalledTimes(1)
        expect(spawn).toHaveBeenCalledWith(
          'no-spaces',
          expect.objectContaining({
            shell: false
          })
        )
      })
    })

    it('should spawn the command detached so that the subprocess group can be killed', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(spawn).toHaveBeenCalledTimes(1)
      expect(spawn).toHaveBeenCalledWith(
        spec.command,
        expect.objectContaining({
          detached: true
        })
      )
    })

    it('should spawn the command from the given directory', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(spawn).toHaveBeenCalledTimes(1)
      expect(spawn).toHaveBeenCalledWith(
        spec.command,
        expect.objectContaining({
          cwd: spec.pwd
        })
      )
    })

    it('should use the provided path to spawn the command', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(spawn).toHaveBeenCalledTimes(1)
      expect(spawn).toHaveBeenCalledWith(
        spec.command,
        expect.objectContaining({
          env: expect.objectContaining({
            PATH: spec.path
          })
        })
      )
    })

    it('should ignore stdout and stderr by default', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(spawn).toHaveBeenCalledTimes(1)
      expect(spawn).toHaveBeenCalledWith(
        spec.command,
        expect.objectContaining({
          stdio: [
            'ignore',
            'ignore',
            'ignore'
          ]
        })
      )
    })

    it('should use the provided pipe for stdout', () => {
      spec.stdout = { pipe: jest.fn() }
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(spawn).toHaveBeenCalledTimes(1)
      expect(spawn).toHaveBeenCalledWith(
        spec.command,
        expect.objectContaining({
          stdio: [
            'ignore',
            'pipe',
            'ignore'
          ]
        })
      )
      expect(child.stdout.pipe).toHaveBeenCalledTimes(1)
      expect(child.stdout.pipe).toHaveBeenCalledWith(
        spec.stdout,
        expect.objectContaining({
          end: false
        })
      )
    })

    it('should use the provided pipe for stderr', () => {
      spec.stderr = { pipe: jest.fn() }
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(spawn).toHaveBeenCalledTimes(1)
      expect(spawn).toHaveBeenCalledWith(
        spec.command,
        expect.objectContaining({
          stdio: [
            'ignore',
            'ignore',
            'pipe'
          ]
        })
      )
      expect(child.stderr.pipe).toHaveBeenCalledTimes(1)
      expect(child.stderr.pipe).toHaveBeenCalledWith(
        spec.stderr,
        expect.objectContaining({
          end: false
        })
      )
    })

    it('should start a timeout which kills the child', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()

      expect(jest.getTimerCount()).toBe(1)
      expect(process.kill).not.toHaveBeenCalled()

      jest.runAllTimers()

      expect(jest.getTimerCount()).toBe(0)
      expect(process.kill).toHaveBeenCalledTimes(1)
      expect(process.kill).toHaveBeenCalledWith(
        -child.pid,
        runner.killSignal
      )
    })

    it('should ignore errors calling process.kill', () => {
      process.kill.mockImplementation(() => {
        throw new Error('pid does not exist')
      })

      expect(() => {
        const runner = new Runner(spec, mockLogger())
        runner.runCommand()
        jest.runAllTimers()
      }).not.toThrow()

      expect(process.kill).toHaveBeenCalledTimes(1)
    })

    it('should record the child command details', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(runner.current.promise).toBeInstanceOf(Promise)
      expect(runner.current.child).toBe(child)
      expect(typeof runner.current.kill).toBe('function')
    })

    it('should emit that runner has updated', () => {
      const onUpdated = jest.fn()
      const runner = new Runner(spec, mockLogger())
      runner.on('updated', onUpdated)
      runner.runCommand()
      expect(onUpdated).toHaveBeenCalledTimes(1)
      expect(onUpdated).toHaveBeenCalledWith(runner)
    })

    it('should bind to child exit and error', () => {
      const runner = new Runner(spec, mockLogger())
      runner.runCommand()
      expect(child.on).toHaveBeenCalledTimes(2)
      expect(child.on).toHaveBeenCalledWith('error', expect.any(Function))
      expect(child.on).toHaveBeenCalledWith('exit', expect.any(Function))
    })

    describe('on command exit', () => {
      let runner
      let onExit
      let onError
      let exitCode
      let signal
      let spawnError
      const mockedState = 'someState'
      const mockedCode = 213
      const mockedCodeClass = 'goodEnough'
      const mockedResult = 'yass'

      beforeEach(() => {
        exitCode = undefined
        signal = undefined
        spawnError = undefined

        runner = new Runner(spec, mockLogger())

        runner.determineState = jest.fn()
        runner.determineState.mockReturnValue({
          state: mockedState,
          code: mockedCode
        })

        runner.determineCodeClass = jest.fn()
        runner.determineCodeClass.mockReturnValue(mockedCodeClass)

        runner.interpretResult = jest.fn()
        runner.interpretResult.mockReturnValue(mockedResult)

        runner.onCommandComplete = jest.fn()

        runner.runCommand()

        onExit = child.on.mock.calls.find(call => call[0] === 'exit')[1]
        onError = child.on.mock.calls.find(call => call[0] === 'error')[1]
      })

      describe.each([
        {
          reason: 'normal exit',
          simulate: () => {
            exitCode = 125
            signal = null
            onExit(exitCode, signal)
          }
        },
        {
          reason: 'killed by signal',
          simulate: () => {
            exitCode = null
            signal = 'SIGHUP'
            onExit(exitCode, signal)
          }
        },
        {
          reason: 'error spawning',
          simulate: () => {
            spawnError = new Error('unable to spawn command')
            onError(spawnError)
          }
        }
      ])('due to $reason', ({ simulate }) => {
        it('should clear the timeout', () => {
          expect(jest.getTimerCount()).toBe(1)
          simulate()
          expect(jest.getTimerCount()).toBe(0)
        })

        it('should record the end time', async () => {
          simulate()
          expect(Date.now).toHaveBeenCalledTimes(2)
          const resolved = await runner.current.promise
          expect(resolved.ended).toBe(Date.now.mock.results[1].value)
        })

        it('should use determineState for the resolved values', async () => {
          simulate()
          const resolved = await runner.current.promise

          expect(runner.determineState).toHaveBeenCalledTimes(1)
          expect(runner.determineState).toHaveBeenCalledWith(
            expect.objectContaining({
              err: signal !== undefined ? signal : spawnError,
              timedOut: false,
              code: exitCode
            })
          )
          expect(resolved.state).toBe(mockedState)
          expect(resolved.code).toBe(mockedCode)
        })

        it('should use determineCodeClass for the resolved values', async () => {
          simulate()
          const resolved = await runner.current.promise

          expect(runner.determineCodeClass).toHaveBeenCalledTimes(1)
          expect(runner.determineCodeClass).toHaveBeenCalledWith(mockedCode)
          expect(resolved.codeClass).toBe(mockedCodeClass)
        })

        it('should use interpretResult for the resolved values', async () => {
          simulate()
          const resolved = await runner.current.promise

          expect(runner.interpretResult).toHaveBeenCalledTimes(1)
          expect(runner.interpretResult).toHaveBeenCalledWith(
            expect.objectContaining({
              state: mockedState,
              code: mockedCode,
              codeClass: mockedCodeClass
            })
          )
          expect(resolved.result).toBe(mockedResult)
        })

        it('should call onCommandComplete', async () => {
          expect(runner.onCommandComplete).toHaveBeenCalledTimes(0)
          simulate()
          await runner.current.promise
          expect(runner.onCommandComplete).toHaveBeenCalledTimes(1)
        })
      })
    })
  })

  describe('onCommandComplete', () => {
    let resolved
    let runner

    beforeEach(() => {
      jest.useFakeTimers()

      resolved = {
        state: Runner.EXIT_STATE_EXITED,
        code: 100,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        result: Runner.EXIT_RESULT_MET,
        started: 1000,
        ended: 1050
      }

      runner = new Runner({ latch: false }, mockLogger())
      runner.clearDelay = jest.fn()
      runner.runCommand = jest.fn()
      runner.stop = jest.fn()

      runner.shouldKeepGoing = jest.fn()
      runner.shouldKeepGoing.mockReturnValue(true)
    })

    afterEach(() => {
      jest.clearAllTimers()
      jest.useRealTimers()
    })

    it('should clear the current command', () => {
      runner.current = { test: 123 }
      runner.onCommandComplete(resolved)
      expect(runner.current).toBe(null)
    })

    describe('when stopped', () => {
      beforeEach(() => {
        runner.running = false
      })

      it('should not affect any results', () => {
        const expected = {
          consecutive: 100,
          last: 'last',
          firstMet: 123,
          delayHandle: null
        }
        Object.assign(runner, expected)

        const onUpdated = jest.fn()
        runner.on('updated', onUpdated)

        runner.onCommandComplete(resolved)

        expect(jest.getTimerCount()).toBe(0)
        expect(runner).toMatchObject(expected)
        expect(runner.clearDelay).not.toHaveBeenCalled()
        expect(runner.runCommand).not.toHaveBeenCalled()
        expect(onUpdated).not.toHaveBeenCalled()
      })
    })

    describe('when started', () => {
      beforeEach(() => {
        runner.running = true
      })

      describe('with last run having a different state', () => {
        it('should reset consecutive count', () => {
          runner.last = {
            state: 'somethingElse',
            result: resolved.result
          }

          runner.onCommandComplete(resolved)

          expect(runner.consecutive).toBe(1)
        })
      })

      describe('with last run having a different result', () => {
        it('should reset consecutive count', () => {
          runner.last = {
            state: resolved.state,
            result: 'differentResult'
          }

          runner.onCommandComplete(resolved)

          expect(runner.consecutive).toBe(1)
        })
      })

      describe('with last run having same state and result', () => {
        it('should increment the consecutive count', () => {
          runner.last = {
            state: resolved.state,
            result: resolved.result
          }
          runner.consecutive = 50

          runner.onCommandComplete(resolved)

          expect(runner.consecutive).toBe(51)
        })
      })

      describe('with result unmet', () => {
        beforeEach(() => {
          resolved.result = Runner.EXIT_RESULT_UNMET
        })

        it('should reset the firstMet time', () => {
          runner.firstMet = 12345

          runner.onCommandComplete(resolved)

          expect(runner.firstMet).toBe(null)
        })
      })

      describe('with result met', () => {
        beforeEach(() => {
          resolved.result = Runner.EXIT_RESULT_MET
        })

        describe('and a previous firstMet value', () => {
          beforeEach(() => {
            runner.firstMet = 12345
          })

          it('should not modify the firstMet value', () => {
            runner.onCommandComplete(resolved)

            expect(runner.firstMet).toBe(12345)
          })
        })

        describe('and no previous firstMet value', () => {
          beforeEach(() => {
            runner.firstMet = null
          })

          it('should set the firstMet value', () => {
            runner.onCommandComplete(resolved)

            expect(runner.firstMet).toBe(resolved.ended)
          })
        })
      })

      describe('with result ignore', () => {
        beforeEach(() => {
          resolved.result = Runner.EXIT_RESULT_IGNORE
        })

        it('should not alter the firstMet value', () => {
          runner.firstMet = 'firstMet'

          runner.onCommandComplete(resolved)

          expect(runner.firstMet).toBe('firstMet')
        })
      })

      describe('without latching', () => {
        it('should schedule the next run', () => {
          expect(jest.getTimerCount()).toBe(0)

          runner.onCommandComplete(resolved)

          expect(runner.stop).not.toHaveBeenCalled()
          expect(jest.getTimerCount()).toBe(1)
          expect(runner.clearDelay).not.toHaveBeenCalled()
          expect(runner.runCommand).not.toHaveBeenCalled()

          jest.runAllTimers()

          expect(jest.getTimerCount()).toBe(0)
          expect(runner.clearDelay).toHaveBeenCalledTimes(1)
          expect(runner.runCommand).toHaveBeenCalledTimes(1)
        })
      })

      describe('with latching', () => {
        beforeEach(() => {
          runner.spec.latch = true
        })

        describe('with a result that should keep going', () => {
          it('should schedule the next run', () => {
            expect(jest.getTimerCount()).toBe(0)

            runner.onCommandComplete(resolved)

            expect(runner.stop).not.toHaveBeenCalled()
            expect(jest.getTimerCount()).toBe(1)
            expect(runner.clearDelay).not.toHaveBeenCalled()
            expect(runner.runCommand).not.toHaveBeenCalled()

            jest.runAllTimers()

            expect(jest.getTimerCount()).toBe(0)
            expect(runner.clearDelay).toHaveBeenCalledTimes(1)
            expect(runner.runCommand).toHaveBeenCalledTimes(1)
          })
        })

        describe('with a result that should not keep going', () => {
          it('should stop the runner altogether', () => {
            runner.shouldKeepGoing.mockReturnValue(false)

            expect(jest.getTimerCount()).toBe(0)

            runner.onCommandComplete(resolved)

            expect(jest.getTimerCount()).toBe(0)
            expect(runner.stop).toHaveBeenCalledTimes(1)
          })
        })
      })
    })
  })

  describe('clearDelay', () => {
    let resolved
    let runner

    beforeEach(() => {
      jest.useFakeTimers()

      resolved = {
        state: Runner.EXIT_STATE_EXITED,
        code: 100,
        codeClass: Runner.EXIT_CODE_CLASS_FAIL,
        result: Runner.EXIT_RESULT_MET,
        started: 1000,
        ended: 1050
      }

      runner = new Runner({}, mockLogger())
      runner.runCommand = jest.fn()
      runner.running = true
      runner.shouldKeepGoing = jest.fn(() => true)
    })

    afterEach(() => {
      jest.clearAllTimers()
      jest.useRealTimers()
    })

    it('should remove the scheduled run', () => {
      expect(jest.getTimerCount()).toBe(0)

      runner.onCommandComplete(resolved)

      expect(jest.getTimerCount()).toBe(1)
      expect(runner.runCommand).not.toHaveBeenCalled()

      runner.clearDelay()

      expect(jest.getTimerCount()).toBe(0)
      expect(runner.delayHandle).toBe(null)
      expect(runner.runCommand).not.toHaveBeenCalled()
    })
  })

  describe('start', () => {
    let runner

    beforeEach(() => {
      runner = new Runner({}, mockLogger())
      runner.runCommand = jest.fn()
    })

    describe('when not yet running', () => {
      it('should start running', () => {
        expect(runner.running).toBe(false)

        runner.start()

        expect(runner.running).toBe(true)
        expect(runner.runCommand).toHaveBeenCalledTimes(1)
      })
    })

    describe('when already running', () => {
      it('should do nothing', () => {
        runner.running = true

        runner.start()

        expect(runner.runCommand).toHaveBeenCalledTimes(0)
      })
    })
  })

  describe('stop', () => {
    let kill
    let runner
    let onUpdated

    beforeEach(() => {
      kill = jest.fn()

      runner = new Runner({}, mockLogger())
      runner.clearDelay = jest.fn()
      runner.running = true
      runner.current = { kill }

      onUpdated = jest.fn()
      runner.on('updated', onUpdated)
    })

    describe('when running', () => {
      it('should set running false', () => {
        runner.stop()
        expect(runner.running).toBe(false)
      })

      describe('when a command is running', () => {
        it('should kill the current command', () => {
          runner.stop()
          expect(kill).toHaveBeenCalledTimes(1)
        })

        it('should clear the current command', () => {
          runner.stop()
          expect(runner.current).toBe(null)
        })
      })

      describe('when a command is not running', () => {
        it('should safely skip the kill', () => {
          runner.current = null
          expect(() => {
            runner.stop()
          }).not.toThrow()
        })
      })

      it('should unschedule the next command', () => {
        runner.stop()
        expect(runner.clearDelay).toHaveBeenCalledTimes(1)
      })

      it('should emit that runner has updated', () => {
        runner.stop()
        expect(onUpdated).toHaveBeenCalledTimes(1)
      })
    })

    describe('when already stopped', () => {
      it('should not throw any errors', () => {
        expect(() => {
          runner.stop()
          runner.stop()
        }).not.toThrow()
      })
    })
  })

  describe('toggle', () => {
    let runner

    beforeEach(() => {
      runner = new Runner({}, mockLogger())
      runner.start = jest.fn()
      runner.stop = jest.fn()
    })

    it('should start running when stopped', () => {
      runner.running = false
      runner.toggle()
      expect(runner.start).toHaveBeenCalledTimes(1)
      expect(runner.stop).toHaveBeenCalledTimes(0)
    })

    it('should stop running when started', () => {
      runner.running = true
      runner.toggle()
      expect(runner.start).toHaveBeenCalledTimes(0)
      expect(runner.stop).toHaveBeenCalledTimes(1)
    })
  })
})
