const blessed = require('post-blessed')

const { Runner } = require('./Runner')
const { formatRuntime } = require('./util')

class Interactive {
  constructor ({ started, options, runners, exit }) {
    this.started = started
    this.options = options
    this.runners = runners
    this.exit = exit

    this.initInternal()

    this.update = this.update.bind(this)
    this.onSelect = this.onSelect.bind(this)
    this.onKeypress = this.onKeypress.bind(this)
  }

  initInternal () {
    this.screen = null
    this.table = null
    this.nextAllowedUpdate = 0
    this.nextUpdate = null

    this.showConsecutive = Boolean(
      this.runners.find(
        runner => runner.spec.mode === 'info'
      ) ||
      this.runners.find(
        runner => runner.spec.consecutive > 1
      )
    )
  }

  setup () {
    this.screen = blessed.screen({
      smartCSR: true, // Prevents massive screen updates
      useBCE: true, // May help with some terminals
      dockBorders: true // Looks better
    })

    this.table = blessed.listtable({
      // Attach
      parent: this.screen,

      // Full screen
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',

      // Accept input so scrolling works
      keys: true,
      vi: true,

      // Make scrollable
      scrollable: true,
      scrollbar: {
        style: { bg: 'yellow' },
        track: { bg: 'blue' }
      },

      // Don't override styles for selcted row (that way selected row status colors
      // can be seen)
      invertSelected: false,

      // Support formatting text contents of cells
      tags: true,

      // Overall style
      border: { type: 'line' },
      style: {
        border: { fg: 'white' }
      }
    })

    // Register handler for selection to toggle running
    this.table.on('select', this.onSelect)
    this.table.on('keypress', this.onKeypress)

    // Quit on q, or Control-C.
    // Callback receives (ch, key)
    this.screen.key(['q', 'C-c'], this.exit)

    // Render the screen.
    this.screen.render()

    // Attach to update events
    this.runners.forEach(runner =>
      runner.on('updated', this.update)
    )

    // Kick off first automatic update
    this.update()
  }

  update () {
    // Prevent updates if not ready
    if (!this.screen) {
      return
    }

    // Record time immediately so that logic of this function doesn't affect
    // scheduling
    const now = Date.now()

    // Clear previous timer, whether that was for default rate or because of a
    // runner update
    clearTimeout(this.nextUpdate)

    // If this update would occur too quickly
    if (now < this.nextAllowedUpdate) {
      // Update timer to next allowed update time
      this.nextUpdate = setTimeout(this.update, this.nextAllowedUpdate - now)

      // And prevent immediate update
      return
    }

    // We're going to perform an immediate update, so schedule the next update
    // based on default rate
    this.nextUpdate = setTimeout(this.update, this.options.rate)

    // Calculate next allowable update
    this.nextAllowedUpdate = now + this.options.limit

    // Perform immediate update
    this._update()
  }

  _update () {
    const now = Date.now()

    const tableContents = this.runners.map((runner, index) => {
      const row = []

      // Command
      row.push(blessed.escape(runner.spec.label))

      // State: MET / UNMET
      row.push({
        [Runner.EXIT_RESULT_MET]: this.options.metterm,
        [Runner.EXIT_RESULT_IGNORE]: 'N/A'
      }[runner?.last?.result] || this.options.unmetterm)

      // Last execution result / code
      if (runner?.last) {
        if (runner.last.state === Runner.EXIT_STATE_TIMEOUT) {
          row.push('timeout')
        } else if (runner.last.state === Runner.EXIT_STATE_EXITED) {
          row.push(String(runner.last.code))
        } else {
          row.push('ERROR')
        }
      } else {
        row.push('')
      }

      // Last execution runtime
      row.push(
        (
          (
            runner.last &&
            formatRuntime(
              runner.last.started,
              runner.last.ended
            )
          ) ||
          ''
        ).padStart(8, ' ')
      )

      // Consecutive count
      if (this.showConsecutive) {
        row.push(
          (runner.consecutive < runner.spec.consecutive ? '{red-fg}' : '') +
          String(runner.consecutive).padStart(5, ' ') +
          (runner.consecutive < runner.spec.consecutive ? '{/red-fg}' : '')
        )
      }

      // Current execution runtime
      if (runner.current) {
        row.push(formatRuntime(runner.current.started, now).padStart(8, ' '))
      } else if (!runner.running) {
        row.push('{yellow-fg}PAUSED{/yellow-fg}')
      } else {
        row.push(''.padStart(8, ' '))
      }

      // Since
      row.push(formatRuntime(this.started, (runner.firstMet || now)).padStart(8, ' '))

      let boldPrefix = ''; let boldSuffix = ''
      if (this.table.selected - 1 === index) {
        boldPrefix = '{bold}'
        boldSuffix = '{/bold}'
      }

      let colorPrefix = ''; let colorSuffix = ''
      if (runner?.last?.result === Runner.EXIT_RESULT_MET) {
        [colorPrefix, colorSuffix] = ['{green-fg}', '{/green-fg}']
      } else if (runner?.last?.result === Runner.EXIT_RESULT_UNMET) {
        [colorPrefix, colorSuffix] = ['{red-fg}', '{/red-fg}']
      } else if (runner?.last?.result === Runner.EXIT_RESULT_IGNORE && runner?.last?.codeClass === Runner.EXIT_CODE_CLASS_PASS) {
        [colorPrefix, colorSuffix] = ['{green-fg}', '{/green-fg}']
      } else if (runner?.last?.result === Runner.EXIT_RESULT_IGNORE && runner?.last?.codeClass === Runner.EXIT_CODE_CLASS_FAIL) {
        [colorPrefix, colorSuffix] = ['{red-fg}', '{/red-fg}']
      } else {
        [colorPrefix, colorSuffix] = ['{cyan-fg}', '{/cyan-fg}']
      }

      row.forEach((cell, index) => {
        row[index] = colorPrefix + boldPrefix + cell + boldSuffix + colorSuffix
      })

      return row
    })

    const header = [
      '{bold}{underline}' + blessed.escape(this.options.commandterm) + '{/}',
      '{bold}{underline}State{/}',
      '{bold}{underline}Code{/}',
      '{bold}{underline}Last{/}',
      '{bold}{underline}Count{/}', // only if this.showConsecutive
      '{bold}{underline}This{/}',
      '{bold}{underline}Since{/}'
    ]
    if (!this.showConsecutive) {
      header.splice(4, 1)
    }
    tableContents.unshift(header)

    this.table.setData(tableContents)
    this.screen.render()
  }

  onKeypress (ch, key) {
    // Alias space key to also select item
    if (key.name === 'space') {
      this.table.enterSelected()
    }
  }

  onSelect (row, rowNumber) {
    this.runners[rowNumber - 1]?.toggle()
  }

  shutdown () {
    this.runners.forEach(runner =>
      runner.off('updated', this.update)
    )

    this.screen?.destroy()

    this.initInternal()
  }
}

module.exports = { Interactive }
