/* eslint-env jest */
const { Runner } = require('../Runner')

class AppSimulation {
  constructor () {
    // Always simulate a non-zero start time
    this.started = 1

    // Purposefully pick values that don't match defaults
    this.options = {
      tty: false,
      color: true,
      report: 15,
      rate: 500,
      limit: 200,
      verbose: true,
      commandterm: 'cmd',
      metterm: 'WOOHOO',
      unmetterm: 'sadness',
      waitterm: 'Well?'
    }

    // Default winston log levels
    this.logger = {
      log: jest.fn(),
      silly: jest.fn(),
      debug: jest.fn(),
      verbose: jest.fn(),
      http: jest.fn(),
      info: jest.fn(),
      warn: jest.fn(),
      error: jest.fn()
    }

    this.fileDescriptors = []
    this.allPrefixStreams = []
    this.allPipesFinishedPromise = Promise.resolve()

    // One runner that hasn't received its first result yet
    const runnerPendingFirstResult = new Runner()
    runnerPendingFirstResult.running = true
    runnerPendingFirstResult.spec = {
      label: 'pendingFirstResult',
      consecutive: 1
    }
    runnerPendingFirstResult.current = {
      promise: Promise.resolve(),
      child: {},
      kill: jest.fn(),
      started: this.started
    }
    runnerPendingFirstResult.last = null
    runnerPendingFirstResult.consecutive = 0

    // One runner that was stopped/paused before receiving its first result
    const runnerStopped = new Runner()
    runnerStopped.running = false
    runnerStopped.spec = {
      label: 'paused',
      consecutive: 1
    }
    runnerStopped.current = null
    runnerStopped.last = null
    runnerStopped.consecutive = 0

    // Create a set of runners in various states for testing
    this.runners = [
      runnerPendingFirstResult,
      runnerStopped
    ]

    // Permutations
    const consecutiveTargets = [1, 2]
    const consecutiveValues = [1, 2]
    const runningStates = [false, true]
    const exitCodes = [0, 127]
    const modes = ['pass', 'fail', 'info']
    consecutiveValues.forEach(consecutiveValue => {
      const createRunner0 = () => {
        const runner = new Runner()
        runner.spec = {
          // Make the label an array so we can build it up easily as we go
          label: ['consecutive_' + consecutiveValue],
          consecutive: 1,
          mode: 'pass'
        }
        runner.running = true
        runner.last = {
          state: Runner.EXIT_STATE_EXITED,
          code: 0,
          codeClass: Runner.EXIT_CODE_CLASS_PASS,
          result: Runner.EXIT_RESULT_MET,
          started: this.started,
          ended: this.started
        }
        runner.consecutive = consecutiveValue
        runner.current = {
          promise: Promise.resolve(),
          child: {},
          kill: jest.fn(),
          started: this.started
        }
        runner.delayHandle = null
        runner.firstMet = null

        return runner
      }

      consecutiveTargets.forEach(consecutiveTarget => {
        const createRunner1 = () => {
          const runner = createRunner0()
          runner.spec.label.push('of_' + consecutiveTarget)
          runner.spec.consecutive = consecutiveTarget
          return runner
        }

        runningStates.forEach(runningState => {
          const createRunner2 = () => {
            const runner = createRunner1()
            runner.spec.label.push(runningState ? 'running' : 'delaying')
            if (!runningState) {
              runner.current = null
              runner.delayHandle = true
            }
            return runner
          }

          // EXIT_STATE_EXITED
          exitCodes.forEach(exitCode => {
            const createRunner3 = () => {
              const runner = createRunner2()
              runner.spec.label.push('exited_' + exitCode)
              runner.last.state = Runner.EXIT_STATE_EXITED
              runner.last.code = exitCode
              return runner
            }

            modes.forEach(mode => {
              const createRunner4 = () => {
                const runner = createRunner3()
                runner.spec.label.push('mode_' + mode)
                runner.spec.mode = mode
                return runner
              }

              this.runners.push(createRunner4())
            })
          })

          // EXIT_STATE_TIMEOUT
          const createTimedoutRunner = () => {
            const runner = createRunner2()
            runner.spec.label.push('timedout')
            runner.last.state = Runner.EXIT_STATE_TIMEOUT
            runner.last.code = NaN
            return runner
          }
          this.runners.push(createTimedoutRunner())

          // EXIT_STATE_OTHER
          const createErroredRunner = () => {
            const runner = createRunner2()
            runner.spec.label.push('errored')
            runner.last.state = Runner.EXIT_STATE_OTHER
            runner.last.code = NaN
            return runner
          }
          this.runners.push(createErroredRunner())
        })
      })
    })

    // Convert the labels back into strings where we generated them from
    // permutation
    this.runners.forEach(runner => {
      runner.start = jest.fn()
      runner.stop = jest.fn()
      runner.toggle = jest.fn()

      if (Array.isArray(runner.spec.label)) {
        runner.spec.label = runner.spec.label.join('_')
      }

      if (runner.last) {
        runner.last.codeClass = runner.determineCodeClass(
          runner.last.code
        )

        runner.last.result = runner.interpretResult({
          state: runner.last.state,
          code: runner.last.code,
          codeClass: runner.last.codeClass
        })
      }
    })

    this.display = null
    this.killed = false
    this.allRunnersInfo = false

    this.exit = jest.fn()
  }

  updateEndTimes (ended) {
    this.runners.forEach(runner => {
      if (runner.last) {
        runner.last.ended = ended
      }
    })
  }

  removeDelayRunners () {
    this.runners = this.runners.filter(runner => Boolean(runner.current))
  }

  simulateNoConsecutiveTargets () {
    this.runners = this.runners.filter(runner => runner.spec.consecutive === 1)
  }

  simulateNoInfoRunners () {
    this.runners = this.runners.filter(runner => runner.spec.mode !== 'info')
  }

  exit () {
    this.killed = true
    this.display?.shutdown()
  }
}

module.exports = { AppSimulation }
