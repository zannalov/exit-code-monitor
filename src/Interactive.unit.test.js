/* eslint-env jest */
jest.mock('post-blessed')
jest.mock('./util')

const blessed = require('post-blessed')

const { AppSimulation } = require('./__fixtures__/AppSimulation')
const { formatRuntime } = require('./util')

const subject = require('./Interactive')

const ended = 2346
let app

beforeEach(() => {
  jest.useFakeTimers()

  // Spy on date so we can verify calculations
  jest.spyOn(Date, 'now')

  // Show that we called the method instead of showing the formatted result
  // (assume the testing of the method covers actual formatting)
  formatRuntime.mockReset()
  formatRuntime.mockImplementation(
    (started, ended) => '{{formatRuntime}}'
  )

  // Simulate app
  app = new AppSimulation()
  app.updateEndTimes(ended)
})

afterEach(() => {
  Date.now.mockRestore()

  jest.clearAllTimers()
  jest.useRealTimers()
})

describe('Interactive', () => {
  let screen
  let table

  beforeEach(() => {
    screen = {
      key: jest.fn(),
      render: jest.fn(),
      destroy: jest.fn()
    }

    blessed.screen.mockReset()
    blessed.screen.mockReturnValue(screen)

    table = {
      setData: jest.fn(),
      on: jest.fn(),
      enterSelected: jest.fn()
    }

    blessed.listtable.mockReset()
    blessed.listtable.mockReturnValue(table)

    blessed.escape.mockReset()
    blessed.escape.mockImplementation(str => `%[${str}]%`)
  })

  describe('constructor', () => {
    it('should cache app details', () => {
      const display = new subject.Interactive(app)
      expect(display.started).toBe(app.started)
      expect(display.options).toBe(app.options)
      expect(display.runners).toBe(app.runners)
      expect(display.exit).toBe(app.exit)
    })

    it('should not affect anything until started', () => {
      /* eslint-disable no-unused-vars */
      const display = new subject.Interactive(app)
      /* eslint-enable no-unused-vars */

      expect(blessed.screen).not.toHaveBeenCalled()
      expect(blessed.listtable).not.toHaveBeenCalled()
      expect(blessed.escape).not.toHaveBeenCalled()
    })
  })

  describe('initInternal', () => {
    it('should reset local variables', () => {
      const display = new subject.Interactive(app)
      display.screen = 'screen'
      display.table = 'table'
      display.nextAllowedUpdate = 12345
      display.nextUpdate = 'timeoutHandle'

      display.initInternal()

      expect(display).toMatchObject({
        screen: null,
        table: null,
        nextAllowedUpdate: 0,
        nextUpdate: null
      })
    })

    it('should not show consecutive counts if they all use defaults', () => {
      app.simulateNoConsecutiveTargets()
      app.simulateNoInfoRunners()

      const display = new subject.Interactive(app)

      expect(display.showConsecutive).toBe(false)
    })

    it('should show consecutive counts if some runners have consecutive targets', () => {
      // app.simulateNoConsecutiveTargets()
      app.simulateNoInfoRunners()

      const display = new subject.Interactive(app)

      expect(display.showConsecutive).toBe(true)
    })

    it('should show consecutive counts if some runners have mode=info', () => {
      app.simulateNoConsecutiveTargets()
      // app.simulateNoInfoRunners()

      const display = new subject.Interactive(app)

      expect(display.showConsecutive).toBe(true)
    })
  })

  describe('setup', () => {
    it('should initialize the blessed screen and table', () => {
      const display = new subject.Interactive(app)
      display.update = jest.fn()

      display.setup()

      expect(blessed.screen).toHaveBeenCalledTimes(1)
      expect(blessed.listtable).toHaveBeenCalledTimes(1)
      expect(screen.render).toHaveBeenCalledTimes(1)
      expect(display.update).toHaveBeenCalledTimes(1)
    })

    it('should bind the q and ctrl-c keys to app.exit', () => {
      const display = new subject.Interactive(app)
      display.update = jest.fn()

      display.setup()

      expect(screen.key).toHaveBeenCalledTimes(1)
      expect(screen.key).toHaveBeenCalledWith(['q', 'C-c'], app.exit)
    })

    it('should trigger display updates when the commands update', () => {
      const display = new subject.Interactive(app)
      display.update = jest.fn()

      display.setup()

      display.update.mockClear()
      app.runners[0].emit('updated')
      expect(display.update).toHaveBeenCalledTimes(1)
    })
  })

  describe('update', () => {
    it('should ignore updates when screen is not ready', () => {
      const display = new subject.Interactive(app)
      display._update = jest.fn()
      display.update()

      expect(jest.getTimerCount()).toBe(0)
      expect(display.nextUpdate).toBe(null)
      expect(display.nextAllowedUpdate).toBe(0)
      expect(display._update).not.toHaveBeenCalled()
    })

    it('should run _update immediately if enough time has passed', () => {
      const display = new subject.Interactive(app)
      display._update = jest.fn()
      jest.spyOn(display, 'update')

      display.setup() // setup calls update

      expect(display.update).toHaveBeenCalledTimes(1)
      expect(display._update).toHaveBeenCalledTimes(1)
    })

    it('should schedule to call itself after options.rate', () => {
      const display = new subject.Interactive(app)
      jest.spyOn(display, 'update')
      display._update = jest.fn()

      display.setup() // setup calls update

      const now = Date.now.mock.results[Date.now.mock.results.length - 1].value
      expect(jest.getTimerCount()).toBe(1)
      expect(display.nextUpdate).not.toBe(null)
      expect(display.nextAllowedUpdate).toBe(now + app.options.limit)

      jest.advanceTimersByTime(app.options.rate)

      expect(display.update).toHaveBeenCalledTimes(2)
    })

    it('should not run _update if an update was performed too recently', () => {
      const display = new subject.Interactive(app)
      jest.spyOn(display, 'update')
      display._update = jest.fn()

      display.setup() // setup calls update

      expect(display._update).toHaveBeenCalledTimes(1)

      display.update()

      expect(display._update).toHaveBeenCalledTimes(1)
    })

    it('should schedule to call itself as soon as allowable', () => {
      const display = new subject.Interactive(app)
      jest.spyOn(display, 'update')
      display._update = jest.fn()

      display.setup() // setup calls update

      expect(display._update).toHaveBeenCalledTimes(1)

      display.update()

      jest.advanceTimersByTime(app.options.limit)

      expect(display._update).toHaveBeenCalledTimes(2)
    })
  })

  describe('_update', () => {
    describe.each([
      {
        name: 'with all permutations',
        setup: () => {}
      },
      {
        name: 'with no consecutive targets and no info mode commands',
        setup: () => {
          app.simulateNoConsecutiveTargets()
          app.simulateNoInfoRunners()
        }
      }
    ])('$name', ({ setup }) => {
      it('should update the table data and re-render', () => {
        setup()

        const display = new subject.Interactive(app)
        display.setup()

        expect(table.setData).toHaveBeenCalledTimes(1)
        expect(table.setData.mock.calls).toMatchSnapshot()
        expect(screen.render).toHaveBeenCalledTimes(2)
      })
    })

    it('should bold the selected row', () => {
      app.simulateNoConsecutiveTargets()
      app.simulateNoInfoRunners()

      const display = new subject.Interactive(app)
      display.setup() // first call to setData
      display.table.selected = 1 // first non-header row
      display._update() // second call to setData

      expect(table.setData).toHaveBeenCalledTimes(2)
      const call = table.setData.mock.calls[1]
      const tableContents = call[0]

      const row = tableContents[1]
      row.forEach(cell => {
        expect(cell).toMatch('{bold}')
      })

      const otherNonHeaderRows = tableContents.slice(2)
      otherNonHeaderRows.forEach(row => {
        row.forEach(cell => {
          expect(cell).not.toMatch('{bold}')
        })
      })
    })
  })

  describe('onKeypress', () => {
    it('should tell the table to select the current item', () => {
      const display = new subject.Interactive(app)
      display.setup()
      expect(table.enterSelected).not.toHaveBeenCalled()

      display.onKeypress('j', { name: 'j' })
      expect(table.enterSelected).not.toHaveBeenCalled()

      display.onKeypress(' ', { name: 'space' })
      expect(table.enterSelected).toHaveBeenCalledTimes(1)
    })
  })

  describe('onSelect', () => {
    it('should toggle the selected runner', () => {
      const display = new subject.Interactive(app)
      display.setup()

      const rowNumber = 1
      const rowData = table.setData.mock.calls[0][rowNumber]

      display.onSelect(rowData, rowNumber)

      expect(app.runners[0].toggle).toHaveBeenCalledTimes(1)
    })
  })

  describe('shutdown', () => {
    it('should destroy the screen', () => {
      const display = new subject.Interactive(app)
      display.setup()
      display.shutdown()

      expect(screen.destroy).toHaveBeenCalledTimes(1)
    })

    it('should clear the internal variables', () => {
      const display = new subject.Interactive(app)
      display.setup()
      display.shutdown()

      expect(display).toMatchObject({
        screen: null,
        table: null,
        nextAllowedUpdate: 0,
        nextUpdate: null
      })
    })

    it('should be safe if run again', () => {
      expect(() => {
        const display = new subject.Interactive(app)
        display.setup()
        display.shutdown()
        display.shutdown()
      }).not.toThrow()
    })
  })
})
