const fs = require('fs')
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const { pick, uniq } = require('lodash')
const winston = require('winston')

const APP_OPTIONS = [
  'tty',
  'color',
  'report',
  'rate',
  'limit',
  'verbose',
  'quiet',
  'commandterm',
  'metterm',
  'unmetterm',
  'waitterm',
  'log',
  'level'
]

const COMMAND_OPTIONS = [
  'mode',
  'code',
  'consecutive',
  'latch',
  'timeout',
  'timeoutcode',
  'delay',
  'pwd',
  'path',
  'stdout',
  'stderr'
]

const CARRY_FORWARD_OPTIONS = [].concat(
  APP_OPTIONS,
  COMMAND_OPTIONS
)

const DEFAULTS = {
  mode: 'pass',
  consecutive: 1,
  latch: false,
  timeout: 30,
  timeoutcode: NaN,
  delay: 1,
  pwd: process.cwd(),
  tty: Boolean(process.stdout.isTTY),
  color: false,
  report: 10,
  rate: 250,
  limit: 100,
  commandterm: 'Command',
  metterm: 'GOOD',
  unmetterm: 'BAD',
  waitterm: 'PENDING',

  // https://stackoverflow.com/a/36746090
  // https://nodejs.org/dist/latest-v14.x/docs/api/process.html#process_process_platform
  log: process.platform === 'win32' ? '\\\\.\\NUL' : '/dev/null',
  level: Object.keys(winston.config.npm.levels).slice(-1)[0]
}

// Map shorthand to full option value
const MODE_SHORTHAND = {
  p: 'pass',
  f: 'fail',
  c: 'code',
  i: 'info'
}

const checkArgs = argv => {
  if (!Number.isNaN(argv.code) && (argv.code < 0 || argv.code > 255)) {
    return 'code must be null or between 0 and 255 inclusive'
  }

  if (Number.isNaN(argv.consecutive) || argv.consecutive < 1) {
    return 'consecutive executions must be >= 1'
  }

  if (Number.isNaN(argv.timeout) || argv.timeout < 1) {
    return 'timeout must be >= 1'
  }

  if (!Number.isNaN(argv.timeoutcode) && (argv.timeoutcode < 0 || argv.timeoutcode > 255)) {
    return 'timeoutcode must be null or between 0 and 255 inclusive'
  }

  if (Number.isNaN(argv.delay) || argv.delay < 1) {
    return 'delay must be >= 1'
  }

  if (!fs.statSync(argv.pwd, { throwIfNoEntry: false })?.isDirectory()) {
    return 'pwd must be a valid directory path'
  }

  if (Number.isNaN(argv.report) || argv.report < 1) {
    return 'report must be >= 1'
  }

  if (Number.isNaN(argv.rate) || argv.rate < 100) {
    return 'rate must be >= 100'
  }

  if (Number.isNaN(argv.limit) || argv.limit < 100) {
    return 'limit must be >= 100'
  }

  if (argv.rate < argv.limit) {
    return 'rate must be >= limit'
  }

  return true
}

const parser = yargs()
  // Allow options to be set via environment variable. For example `mode`
  // becomes `EXIT_CODE_MONITOR_MODE`
  .env('EXIT_CODE_MONITOR')

  // Reject any unknown options
  .strict()

  .parserConfiguration({
    // Necessary to override previous options with updated options
    'duplicate-arguments-array': false,

    // Necessary to keep yargs from consuming all commands as positional parameters
    'halt-at-non-option': true
  })

  // All non-option arguments are consumed as individual commands
  .command({
    command: '*',
    description: 'Each non-option argument is interpreted as a command to be repeatedly executed in a subshell until all provided commands meet their expectations. Commands are run within a subshell. Please be careful of command injection!'
  })

  // Target state options
  .option('mode', {
    alias: 'm',
    type: 'string',
    choices: ['pass', 'p', 'fail', 'f', 'code', 'c', 'info', 'i'],
    description: 'How the commands following this option are expected to exit:\n\n' +
      'pass|p expects a zero exit code.\n\n' +
      'fail|f expects a non-zero exit code (and not timeout).\n\n' +
      'code|c expects a specific exit code (see --code|-c).\n\n' +
      'info|i expects any exit code. The command will be run/displayed, but any exit code allows the monitor to exit. If all commands have this mode, exit-code-monitor will not auto-exit.\n\n'
  })
  .option('code', {
    alias: 'c',
    type: 'number',
    description: 'Command is expected to return this exit code\nNOTE: ONLY USED IF --mode=code'
  })
  .option('consecutive', {
    alias: 'x',
    type: 'number',
    description: 'Command must meet expectations this many times in a row [minimum: 1]'
  })
  .option('latch', {
    alias: 'H',
    type: 'boolean',
    description: 'Command will stop running the first time it meets all of its expectations. In interactive mode it can be restarted by pressing enter or space with the command highlighted.'
  })
  .group(['mode', 'code', 'consecutive', 'latch'], 'Target state')

  // Identification options
  .option('label', {
    alias: 'l',
    type: 'string',
    description: 'Set the short string to show for the next command (only applies to next command)',
    defaultDescription: 'first word of command'
  })
  .group(['label'], 'Identification')

  // Timing options
  .option('timeout', {
    alias: 't',
    type: 'number',
    description: 'After this many seconds send kill signal to command and use timeout exit code [minimum: 1]'
  })
  .option('timeoutcode', {
    type: 'number',
    description: 'When timeout is reached use this exit code (may also be "null" to prevent timeout from matching any expectation)',
    defaultDescription: 'null'
  })
  .option('delay', {
    alias: 'd',
    type: 'number',
    description: 'When command exits, delay this many seconds before re-running command [minimum: 1]'
  })
  .group(['timeout', 'timeoutcode', 'delay'], 'Timing')

  // Command context options
  .option('pwd', {
    alias: 'p',
    type: 'string',
    description: 'Process working directory when starting command',
    defaultDescription: '$(pwd)'
  })
  .normalize('pwd')
  .option('path', {
    alias: 'P',
    type: 'string',
    description: 'Prepend PATH with this string',
    defaultDescription: '$PATH'
  })
  .option('stdout', {
    alias: 'o',
    type: 'string',
    description: 'stdout of commands will be formatted "$title: $stdoutLine" and appended to the specified file',
    defaultDescription: '/dev/null'
  })
  .option('stderr', {
    alias: 'e',
    type: 'string',
    description: 'stderr of commands will be formatted "$title: $stderrLine" and appended to the specified file',
    defaultDescription: '/dev/null'
  })
  .group(['pwd', 'path', 'stdout', 'stderr'], 'Command Context')

  // Output options
  .option('tty', {
    alias: 'T',
    type: 'boolean',
    description: 'Force TTY display on or off',
    defaultDescription: 'detected'
  })
  .option('color', {
    type: 'boolean',
    description: 'Force color when in non-TTY mode'
  })
  .option('report', {
    alias: 'r',
    type: 'number',
    description: 'When not using TTY output, emit a report this frequently in seconds [minimum: 1]'
  })
  .option('rate', {
    alias: 'R',
    type: 'number',
    description: 'When using TTY output, refresh the screen at LEAST every so many milliseconds [minimum: 100]'
  })
  .option('limit', {
    alias: 'L',
    type: 'number',
    description: 'When using TTY output, refresh the screen at MOST every so many milliseconds [minimum: 100]'
  })
  .option('verbose', {
    alias: 'v',
    type: 'boolean',
    conflicts: ['quiet'],
    description: 'When not using TTY output, make each emitted update multiline (default is a single line per udpate)'
  })
  .option('quiet', {
    alias: 'q',
    type: 'boolean',
    conflicts: ['verbose'],
    description: 'Do not show anything on the screen, only return exit code when interrupted or complete'
  })
  .option('commandterm', {
    alias: 'C',
    type: 'string',
    description: 'Use this word to represent the commands specified to be watch.'
  })
  .option('metterm', {
    alias: 'M',
    type: 'string',
    description: 'Use this word to represent commands which are meeting their expectations.'
  })
  .option('unmetterm', {
    alias: 'U',
    type: 'string',
    description: 'Use this word to represent commands that are not meeting thier expectations.'
  })
  .option('waitterm', {
    alias: 'W',
    type: 'string',
    description: 'Use this word when we have not yet received any result for a command.'
  })
  .option('log', {
    alias: 'g',
    type: 'string',
    description: 'Write a jsonl winston log to the provided file name.'
  })
  .option('level', {
    alias: 'G',
    type: 'string',
    description: 'What winston log level to use.',
    choices: Object.keys(winston.config.npm.levels)
  })
  .group(['tty', 'color', 'report', 'rate', 'limit', 'verbose', 'quiet', 'commandterm', 'metterm', 'unmetterm', 'waitterm', 'log', 'level'], 'Output')

  // Generic options
  .alias('help', 'h')
  .version()
  .alias('version', 'V')

  // Additional validation
  .check(checkArgs)

  // Set initial defaults (will be overridden as we go)
  .default(DEFAULTS)

  // A few examples for the CLI
  .epilogue(`
    Examples:

    Wait for all commands to succeed:

    $ npx exit-code-monitor command1 command2 command3

    Wait for all commands to fail:

    $ npx exit-code-monitor --mode=fail command1 command2 command3

    Wait for commands 1, 2, 5, and 6 to succeed, and for 3 and 4 to fail

    $ npx exit-code-monitor \\
      --mode=pass command1 command2 \\
      --mode=fail command3 command4 \\
      --mode=pass command5 command6

    Note that the above can be written more concisely this way, but that the commands will then be listed in the order 1 2 5 6 3 4:

    $ npx exit-code-monitor \\
      command1 command2 command5 command6 \\
      --mode=fail command3 command4

    Wait for the command to have exit code 123 using short options:

    $ npx exit-code-monitor -m c -c 123 command1
  `.replace(/\n {4}/g, '\n').trim())

// Remove empty strings and return each first unique path (so "::a:b:a::c::"
// would become "a:b:c")
const cleanupPath = path => uniq(path.split(':').filter(p => Boolean(p))).join(':')

// Parse process.argv and return the set of commands specified with all options
// relative to that command, as well as the global options for the monitor
const parseArgv = () => {
  // Collectors
  const options = {
    commandSpecs: []
  }

  // Main loop
  let argv = hideBin(process.argv)
  let parsed = { ...DEFAULTS }
  let currentPath = process.env.PATH
  do {
    // Parse remaining arguments up until first non-option
    parsed = parser
      // These values should carry forward to next command
      .default(pick(parsed, CARRY_FORWARD_OPTIONS))
      .parse(argv)

    // If there's a non-option available, that should be a command
    if (parsed._.length) {
      // Remove the command from the list of unparsed arguments
      const command = parsed._.shift()

      // Command must have length
      if (!command) {
        parser.showHelp()
        console.error('commands must not be empty')
        process.exit(1)
      }

      // Add the provided path to the front of the current path, removing
      // duplicates and empties.
      if (parsed.path) {
        currentPath = cleanupPath(parsed.path + ':' + currentPath)
      }

      // If the mode is one of the shorthand versions, expand it
      if (MODE_SHORTHAND[parsed.mode]) {
        parsed.mode = MODE_SHORTHAND[parsed.mode]
      }

      // Add commandSpec object to the collector
      options.commandSpecs.push({
        command,
        label: parsed.label || command.split(' ')[0],
        ...pick(parsed, COMMAND_OPTIONS),
        path: currentPath
      })
    }

    // Update monitoring global options with latest values
    Object.assign(options, pick(parsed, APP_OPTIONS))

    // Replace argv with remaining unparsed options
    argv = parsed._
  } while (argv.length)

  if (!options.commandSpecs.length) {
    parser
      .check(() => { throw new Error('At least one command must be provided') })
      .parse([])
  }

  return options
}

module.exports = {
  APP_OPTIONS,
  CARRY_FORWARD_OPTIONS,
  COMMAND_OPTIONS,
  DEFAULTS,
  MODE_SHORTHAND,
  checkArgs,
  cleanupPath,
  parseArgv,
  parser
}
